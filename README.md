# Git and Gitlab Introduction Tutorial

## Tutorial documentation

- [Tutorial Workbook](tutogit.md)
- [Tutorial Slides](https://notes.inria.fr/p/mEu0XIEpn)

## Git Cheatsheets

- [Git Data Storage](mementos/git_data_transport-880x440.png)
- [Working with Git Workflow](mementos/workflow-of-version-control.pdf)
- [Git Data Storage](mementos/git_data_transport-880x440.png)
- [Git Basic Cheatsheet and Tips](mementos/git-cheatsheet-EN-white.pdf)
- [Git Branching Cheatsheet](mementos/working-with-branches-in-git-cheat-sheet.pdf)
- [Another Git Cheat Sheet all in one](mementos/zt_git_cheat_sheet.pdf)

## Some interesting links

- Github: https://training.github.com
- Gitlab: https://about.gitlab.com/images/press/git-cheat-sheet.pdf
- Interactive Git Cheatsheet: https://ndpsoftware.com/git-cheatsheet.html#loc=workspace
- Interactive and funny tutorial about branching and merging: https://learngitbranching.js.org