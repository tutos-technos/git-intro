"""
This is the "polynomial" module.

The polynomial module supplies one class, Polynomial().  For example,

>>> Polynomial(1, 2, 3)
Polynomial(1, 2, 3)
"""


class Polynomial(object):
    """Implements a single-variable mathematical polynomial."""

    def __init__(self, *args):
        """Initialize the polynomial."""
        self.coefficients = args

    def degree(self):
        """Return the degree of the polynomial."""
        return 0

    def evaluate(self, x):
        """Evaluate the polynomial at a given point."""
        pass

    def derivative(self):
        """Return a polynomial object which is the derivative of self."""
        pass

    def __repr__(self):
        args = ", ".join(str(c) for c in self.coefficients)
        return f"Polynomial({args})"


if __name__ == "__main__":
    import doctest

    doctest.testmod()
